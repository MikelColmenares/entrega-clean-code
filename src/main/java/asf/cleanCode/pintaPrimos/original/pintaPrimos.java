package asf.cleanCode.pintaPrimos.original;

public class pintaPrimos {

    static final int numeroDePrimosQueSeQuiereObtener = 1000;
    static final int Linea = 50;
    static final int Columna = 4;
    static final int kl = 10;
    static final int ordmax = 30;
    static int arrayDeNumerosPrimos[] = new int[numeroDePrimosQueSeQuiereObtener + 1];
    static int NUMERODEPAGINA;
    static int PAGEOFFSET;
    static int ROWOFFSET;
    static int i;
    static int contadorQueSeAlmacena;
    static int contadorDeLosPrimosQueLlevasObtenidos;
    static boolean esPrimo;
    static int contadorDelArrayDeCuadrados;
    static int cuadradoDeUnNumeroPrimo;
    static int contadorDelBucle;
    static int arrayDePrimosEspeciales[] = new int[ordmax + 1];
  public static void pinta() {
    contadorQueSeAlmacena = 1;
    contadorDeLosPrimosQueLlevasObtenidos = 1;
    arrayDeNumerosPrimos[1] = 2;
    contadorDelArrayDeCuadrados = 2;
    cuadradoDeUnNumeroPrimo = 9;

    while (contadorDeLosPrimosQueLlevasObtenidos < numeroDePrimosQueSeQuiereObtener) {
      do {
        rellenarArrayDePrimosEspeciales();
        comprobacionDeSiEsPrimo();
      } while (!esPrimo);

      contadorDeLosPrimosQueLlevasObtenidos = contadorDeLosPrimosQueLlevasObtenidos + 1;
      arrayDeNumerosPrimos[contadorDeLosPrimosQueLlevasObtenidos] = contadorQueSeAlmacena;
    }

    NUMERODEPAGINA = 1;
    PAGEOFFSET = 1;
    while (PAGEOFFSET <= numeroDePrimosQueSeQuiereObtener) {
      System.out.println("Los primeros " + numeroDePrimosQueSeQuiereObtener +
                           " Números primos --- Página " + NUMERODEPAGINA);
      System.out.println("");

      pintaLosPrimosAlmacenadosEnElArray();

      System.out.println("\f");
      NUMERODEPAGINA = NUMERODEPAGINA + 1;
      PAGEOFFSET = PAGEOFFSET + Linea * Columna;
    }
  }

  private static void pintaLosPrimosAlmacenadosEnElArray() {
    for (ROWOFFSET = PAGEOFFSET; ROWOFFSET < PAGEOFFSET + Linea; ROWOFFSET++){
      for (i = 0; i < Columna; i++)
        if (ROWOFFSET + i * Linea <= numeroDePrimosQueSeQuiereObtener)
          System.out.format("%10d", arrayDeNumerosPrimos[ROWOFFSET + i * Linea]);
      System.out.println("");
    }
  }

  private static void comprobacionDeSiEsPrimo() {
    esPrimo = true;
    contadorDelBucle = 2;
    while (contadorDelBucle < contadorDelArrayDeCuadrados && esPrimo) {
      while (arrayDePrimosEspeciales[contadorDelBucle] < contadorQueSeAlmacena)
        arrayDePrimosEspeciales[contadorDelBucle] = arrayDePrimosEspeciales[contadorDelBucle] + arrayDeNumerosPrimos[contadorDelBucle] + arrayDeNumerosPrimos[contadorDelBucle];
      if (arrayDePrimosEspeciales[contadorDelBucle] == contadorQueSeAlmacena)
        esPrimo = false;
      contadorDelBucle = contadorDelBucle + 1;
    }
  }

  private static void rellenarArrayDePrimosEspeciales() {
    contadorQueSeAlmacena = contadorQueSeAlmacena + 2;
    if (contadorQueSeAlmacena == cuadradoDeUnNumeroPrimo) {
      contadorDelArrayDeCuadrados = contadorDelArrayDeCuadrados + 1;
      cuadradoDeUnNumeroPrimo = arrayDeNumerosPrimos[contadorDelArrayDeCuadrados] * arrayDeNumerosPrimos[contadorDelArrayDeCuadrados];
      arrayDePrimosEspeciales[contadorDelArrayDeCuadrados - 1] = contadorQueSeAlmacena;
    }
  }
}