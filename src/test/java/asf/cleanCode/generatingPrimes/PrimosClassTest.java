package asf.cleanCode.generatingPrimes;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PrimosClassTest {

   @Test
   public void generadorDePrimosHasta5() {

        List<Integer> esperado = new ArrayList<>();
        esperado.add(2);
        esperado.add(3);
        esperado.add(5);

       List<Integer> resultado = PrimosClass.generadorDePrimos(5);

       assertEquals(resultado, esperado);
   }


    @Test
    public void generadorDePrimosHasta23() {

        List<Integer> esperado = new ArrayList<>();
        esperado.add(2);
        esperado.add(3);
        esperado.add(5);
        esperado.add(7);
        esperado.add(11);
        esperado.add(13);
        esperado.add(17);
        esperado.add(19);
        esperado.add(23);

        List<Integer> resultado = PrimosClass.generadorDePrimos(23);

        assertEquals(resultado, esperado);
    }


    @Test
    public void generadorDePrimosHasta25() {

        List<Integer> esperado = new ArrayList<>();
        esperado.add(2);
        esperado.add(3);
        esperado.add(5);
        esperado.add(7);
        esperado.add(11);
        esperado.add(13);
        esperado.add(17);
        esperado.add(19);
        esperado.add(23);

        List<Integer> resultado = PrimosClass.generadorDePrimos(25);

        assertEquals(resultado, esperado);
    }


    @Test
    public void generadorDePrimosHasta2() {

        List<Integer> esperado = null;

        List<Integer> resultado = PrimosClass.generadorDePrimos(1);

        assertEquals(resultado, esperado);
    }
}